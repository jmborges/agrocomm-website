import Styles from '../../styles/AboutRow.module.css'
import Image from 'next/image'
export default function aboutRow() {
    return (
        <>
            <section className={Styles.section_aboutRow}>
                <div className={Styles.container_img}>
                    <div className={`${Styles.content_image} ${Styles.image_not_show_mobile}`}> 
                        <Image src='/images/img_1.jpg' width={512} height={493}></Image>
                    </div>
                    <div className={Styles.content_image}>
                        <Image src='/images/img_2.jpg' width={1321} height={493}></Image>
                    </div>
                </div>
            </section> 
            <section className={Styles.container_text}> 
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-lg-6 text-center'>
                            <div className={Styles.content_text}>
                                <div className={Styles.content_text_img}>
                                    <Image src='/images/logo_3.png' width={470} height={118} quality={100}></Image>
                                </div>
                                <div className={`${Styles.content_text_border} ${Styles.about_container}`}></div>
                                <p>Para falar com quem movimenta o agronegócio do plantio a colheita, é importante entender do assunto. Por isso, a Agrocomm conta com uma equipe que tem conhecimento aprofundado e experiência para fazer sua comunicação mais eficiente e eficaz.</p>
                                <p>A <span>Agrocomm</span> é uma <span>Agência de Comunicação e Marketing</span> especializada em <span>Agronegócio,</span> formada por profissionais de comunicação que trabalham sob a supervisão e consultoria de engenheiros agrônomos, fazendo com que a voz da sua marca chegue até seus clientes no tom mais adequado.</p>
                                <p>Se os seus clientes estão no agronegócio, sua agência de comunicação e marketing é a Agrocomm.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
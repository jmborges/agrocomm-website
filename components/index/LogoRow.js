import Styles from '../../styles/LogoRow.module.css'
import Image from 'next/image'
export default function logoRow() {
    return (
        <>
            <section className={Styles.background_logo}>
                <div className={Styles.background_logo_attachment}>
                    <div className='container'>
                        <div className='row justify-content-center'>
                            <div className='col-md-9'>
                                <Image src='/images/logo_2.png' width={600} height={131}/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
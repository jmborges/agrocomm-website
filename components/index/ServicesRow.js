import Styles from '../../styles/ServicesRow.module.css'
export default function servicesRow() {
    return (
        <>
            <section>
                <div className='container'>
                    <div className='row justify-content-center mt-5'>
                        <div className='col-md-4'>
                            <div className={Styles.container_services}>
                                <div className={Styles.content_services_text}>
                                    <p className={Styles.title_services}>
                                        IDENTIDADE VISUAL
                                    </p>
                                    <p className={Styles.text_services}>
                                        A identidade visual de uma marca consiste no nome, logotipo, fonte, cor e estilo próprio. Ela deve transmitir a essência da história da empresa e os seus valores. A identidade visual é imprescindível para a presença da marca tanto nas mídias tradicionais quanto nas digitais, além de ser um dos pilares de sustentação da estratégia de comunicação da marca com o mercado.
                                        A Agrocomm desenvolve projetos de Identidade Visual para marcas através diagnóstico e estudo precisos que levam em conta o perfil da empresa, o mercado de atuação, a linha de produtos/serviços e o perfil do consumidor.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className={Styles.container_services}>
                                <div className={Styles.content_services_text}>
                                    <p className={Styles.title_services}>
                                        PLANEJAMENTO E CRIAÇÃO DE PEÇAS AVULSAS
                                    </p>
                                    <p className={Styles.text_services}>
                                        Também atuamos no planejamento e criação de peças avulsas de comunicação destinadas as mais variadas situações e necessidades de sua empresa. Planejamento e criação de anúncios para mídia impressa, roteiros para mídias sonoras e audiovisuais, assessoramento para seleção e contratação de produtoras de áudio e vídeo, criação de  materiais para divulgação e sinalização de dias de campo e eventos corporativos, entre outras peças.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row justify-content-center'>
                        <div className='col-md-4'> 
                            <div className={Styles.container_services}>
                                <div className={Styles.content_services_text}>
                                    <p className={Styles.title_services}>
                                        PROJETO GRÁFICO E DIAGRAMAÇÃO  DE MATERIAIS EDITORIAIS
                                    </p>
                                    <p className={Styles.text_services}>
                                        O projeto gráfico para divulgação de conteúdos editoriais seja um periódico ou material de ocasião, precisa estar adequado ao tipo de conteúdo e ao público-alvo a que se destina. Se a apresentação visual desses materiais não for adequada, pode prejudicar a comunicação e todo o trabalho de elaboração do conteúdo se perde. É importante que o público-alvo seja atraído visualmente desde o primeiro momento, por isso, um bom projeto gráfico e diagramação são tão importantes.
                                        Trabalhamos em projetos gráficos e diagramação de jornais, revistas, manuais, portfólios, relatórios, livros e materiais técnicos, entre outros. Valorize ainda mais o conteúdo ofertado aos seus clientes com um visual agradável e tecnicamente adequado.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className={Styles.container_services}>
                                <div className={Styles.content_services_text}>
                                    <p className={Styles.title_services}>
                                        BRANDING
                                        (GESTÃO DE MARCAS)
                                    </p>
                                    <p className={Styles.text_services}>
                                        É um conjunto de estratégias que visam tornar a marca mais reconhecida pelo seu público e com presença mais destacada no mercado. Seu objetivo principal é despertar interesse e criar conexões fortes, que serão fatores relevantes para a escolha do consumidor pela sua marca no momento de decisão de compra. A adequada gestão de marcas proporciona o aumento da confiança, potencializa a presença da marca na mente do consumidor e gera fidelidade.
                                        Estamos preparados para ajudar sua empresa/marca a planejar e executar as estratégias necessárias para posicioná-la com eficiência e eficácia nos mercados onde atua.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row justify-content-center'>
                        <div className='col-md-4'>
                            <div className={Styles.container_services}>
                                <div className={Styles.content_services_text}>
                                    <p className={Styles.title_services}>
                                        MARKETING DIGITAL
                                    </p>
                                    <p className={Styles.text_services}>
                                        É o conjunto de atividades que a empresa executa online com o objetivo de atrair novos negócios, criar relacionamentos e desenvolver uma identidade de marca. Podemos ajudar sua empresa a desenvolver estratégias de comunicação adequadas através de um diagnóstico e planejamento da comunicação digital, incluindo a análise de perfil do público-alvo, sempre levando em conta as necessidades e objetivos da sua empresa no ambiente virtual.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className={Styles.container_services}>
                                <div className={Styles.content_services_text}>
                                    <p className={Styles.title_services}>
                                        CONSULTORIA
                                        POR PROJETO
                                        EM MARKETING
                                        E COMUNICAÇÃO
                                    </p>
                                    <p className={Styles.text_services}>
                                        Prestação de serviços de análise, planejamento e orientação para a implementação de estratégias de marketing e comunicação. Uma modalidade de consultoria por projeto desenvolvido, que pode ajudar sua equipe de marketing a planejar, aplicar e gerenciar, com segurança, estratégias e táticas de comunicação adequadas ao seu ramo de atuação.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}    
import Styles from '../../styles/ActingRow.module.css'
export default function actingRow() {
    return (
        <>
            <section className={Styles.background_acting}>
                <div className='container'>
                    <div className='row justify-content-center align-items-end'>
                        <div className='col-md-9 align-self-end'>
                            <h1 className={Styles.title_acting}>
                                NOSSA ATUAÇÃO
                            </h1>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
 }
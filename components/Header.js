import Link from 'next/link'
import Image from 'next/image'
import Li from './nossa-atuacao/Li';
import Styles from '../styles/Header.module.css'
export default function Headers() {
    return (
        <header>
            <div className={Styles.header}>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-md-10'>
                            <nav className="navbar navbar-expand-lg navbar-light">
                                <div className="container">
                                    <Image src='/images/logo.png' width={374} height={93} quality={100} />
                                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                        <span className="navbar-toggler-icon"></span>
                                    </button> 
                                    <div className="collapse navbar-collapse" id="navbarNav">
                                        <ul className="navbar-nav m-auto">
                                            <li className="nav-item">
                                                <Link href="/"><a className={Styles.nav_link}>Home</a></Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link href="/manifesto-agrocomm"><a className={Styles.nav_link} >Manifesto Agrocomm</a></Link>
                                            </li>
                                            <Li/>
                                            <li className="nav-item">
                                                <Link href="/#contato"><a className={Styles.nav_link} >Contato</a></Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}
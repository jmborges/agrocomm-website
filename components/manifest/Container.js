import Styles from '../../styles/Manifest.module.css'
export default function Container() {
    return (
        <>
            <section className={Styles.background_about_repeat}>

            </section>
            <section className={Styles.background_about}>
                <div className={Styles.container_about}>
                    <div className='container'>
                        <div className='row justify-content-center'>
                            <div className='col-md-6'> 
                                <h1 className=''>Manifesto Agrocomm</h1>
                            </div>
                        </div>
                        <div className='row justify-content-center'>
                            <div className='col-md-4'>
                                <div className={Styles.content_text_about}>
                                    <p>Nosso escritório é a lavoura.</p>
                                    <p>Nosso ambiente é o rural.</p>
                                    <p>É nele que nascem as melhores ideias, e a partir dele que se ampliam nossa visão e os diferenciais de atuação nesse setor único como é o agronegócio.</p>
                                    <p>Nosso olhar percorre o ontem, entende o hoje e projeta o amanhã na agricultura e na pecuária.</p>
                                    <p>Conhecemos as etapas dos Sistemas Produtivos, pois sabemos que isso é fundamental para dar vida e alma a uma comunicação bem direcionada e eficaz.</p>
                                    <p>Queremos que nossos clientes falem e sejam compreendidos. Que sua comunicação informe, esclareça, emocione e impacte as pessoas.</p>
                                    <p>Conhecemos a história e somos conscientes dos desafios diários dos homens e mulheres que amam o solo, a semente e a planta que cresce e produz para alimentar o mundo.</p>
                                    <p>Por isso idealizamos a Agrocomm.</p>
                                    <p>Nossa missão é ajudar as empresas a se comunicarem, traçando caminhos bem definidos e criando mensagens que cheguem até a nascente da produção de alimentos, falando com aqueles que estão dentro das porteiras fazendo o nosso agronegócio acontecer.</p>
                                    <p>Se os seus clientes estão no agronegócio, sua agência de Comunicação e Marketing é a Agrocomm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}
import Styles from '../styles/ContactRow.module.css'
import { useState } from 'react'
import axios from 'axios'
export default function ContactRow() {
    const [inputs, setInputs] = useState({
        name: '',
        telephone: '',
        email: '',
        message: '',
    })

    const [form, setForm] = useState('')

    const handleChange = (e) => {
        setInputs((prev) => ({
            ...prev,
            [e.target.id]: e.target.value,
        }))
    }

    const onSubmitForm = async (e) => {
        e.preventDefault()
        if (inputs.name && inputs.email && inputs.message) {
            setForm({ state: 'loading' })
            try {
                const res = await fetch(`api/email`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(inputs),
                })

                const { error } = await res.json()

                if (error) {
                    setForm({
                        state: 'error',
                        message: error,
                    })
                    return
                }

                setForm({
                    state: 'success',
                    message: 'Your message was sent successfully.',
                })
                setInputs({
                    name: '',
                    email: '',
                    message: '',
                })
            } catch (error) {
                setForm({
                    state: 'error',
                    message: 'Something went wrong',
                })
            }
        }
    }
    return (
        <section className={Styles.background_contact} id="contato">
            <div className="container">
                <div className="row justify-content-center">
                    <div className='col-md-10'>
                        <div className={Styles.text_contact}> 
                            <p>Para conhecer mais sobre o nosso trabalho, entre em contato.</p>
                            <span>Estamos à disposição para atender sua empresa e oferecer soluções em marketing e comunicação, adequadas ao perfil do seu produto e dos seus clientes. Agende uma visita presencial ou uma videoconferência.</span>
                        </div>
                    </div>
                    <div className='col-md-11'>
                        <form onSubmit={(e) => onSubmitForm(e)} className={Styles.form_contact}>
                            <div className="row justify-content-center">
                                <div className="col-xs-12 col-md-12 col-sm-12">
                                    <h5 className='text-center'>
                                        Contate-nos
                                    </h5>
                                </div>
                                <div className="col-xs-12 col-md-12 col-sm-12">
                                    <div className={Styles.bbt + ' form-group'}>
                                        <label htmlFor='name'>Nome*</label>
                                        <input id="name" name="name" className="" type="text" autoComplete="Nome" required value={inputs.name} onChange={handleChange} />
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-6 col-sm-6">
                                    <div className={Styles.bbt + ' form-group'}>
                                        <input id="email" name="email" className="" type="email" autoComplete="email" placeholder='E-mail' required value={inputs.email} onChange={handleChange} />
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-6 col-sm-6">
                                    <div className={Styles.bbt + ' form-group'}>
                                        <input id="telephone" name="telephone" className="" type="telephone" autoComplete="telephone" placeholder='Telefone' required value={inputs.telephone} onChange={handleChange} />
                                    </div>
                                </div>
                                <div className="col-xs-12 col-md-12 col-sm-12">
                                    <div className={Styles.bbt + ' form-group'}>
                                        <label htmlFor='message'>Mensagem</label>
                                        <textarea id="message" name="message" className="" cols="30" rows="3" placeholder='Digite aqui sua mensagem' value={inputs.message} onChange={handleChange} />
                                    </div>
                                </div>

                                <div className="col-xs-2 col-md-2 col-sm-2 text-center  mt-2">
                                    <button type='submit' className={Styles.button_contact}>Enviar</button>
                                </div>
                                {form.state === 'loading' ? (
                                    <>
                                        <div className='col-xs-12 col-md-12 col-sm-12'><div className='d-flex text-center justify-content-center align-center'><div className={Styles.loader}></div></div></div>

                                    </>
                                ) : form.state === 'error' ? (
                                    <div className='col-xs-12 col-md-12 col-sm-12 alert alert-danger'>{form.message}</div>
                                ) : (
                                    form.state === 'success' && <div className='col-xs-12 col-md-12 col-sm-12'><div className='text-center'><svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" color='#4CAF50' fill="currentColor" className="bi bi-check-circle" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                                        <path d="M10.97 4.97a.235.235 0 0 0-.02.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-1.071-1.05z" />
                                    </svg></div></div>
                                )}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}
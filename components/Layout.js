import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'
import Script from 'next/script'
export default function Layout({ children }) {
    return (
        <>
            <Head>
                <title>AGROCOMM Agência | Agronegócio | Marketing | Brasil</title>
                <meta name="description" content="A AGROCOMM é uma agência focada em comunicação e marketing para o Agronegócio. Equipe formada por publicitários e agrônomos." />
                <link rel="icon" href="/favicon.ico" />
               
            </Head>
            <Script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous" />
            <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-kjU+l4N0Yf4ZOJErLsIcvOU2qSb74wXpOhqTvwVx3OElZRweTnQ6d31fXEoRD1Jy" crossorigin="anonymous" />
            <Header />
            <main>{children}</main>
            <Footer />
        </>
    )
}
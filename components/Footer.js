import Link from "next/link"
import styles from '../styles/Footer.module.css'
import Image from "next/image"
export default function Footer() {
    return (
        <footer className={styles.footer}>
            <div className='container'>
                <div className='row'>
                    <div className='col-xs-12 col-sm-3 pb-3'>
                        <div className={styles.footer_title}>Menu</div>
                        <ul>
                            <li>
                                <Link href="/" scroll={true}><a >Home</a></Link>
                            </li>
                            <li>
                                <Link href="/manifesto-agrocomm" scroll={true}><a>Manifesto Agrocomm</a></Link>
                            </li>
                            <li>
                                <Link href="/#nossa-atuacao" scroll={true}><a >Nossa Atuação</a></Link>
                            </li>
                            <li>
                                <Link href="/#contato" scroll={true}><a>Contato</a></Link>
                            </li>
                            <li>
                                <Link href="/politica-de-privacidade" scroll={true}><a>Política de privacidade</a></Link>
                            </li>
                        </ul>
                    </div>

                    <div className='col-xs-12 col-sm-3 pb-3'>
                        <div className={styles.footer_title}>Contato</div>
                        <div><a href="https://api.whatsapp.com/send?1=pt_BR&phone=5554996150834" target={'_blank'}>(54) 9 9615-0834</a></div>
                        <div><a href="mailto:atendimento@agrocomm.agr.br">atendimento@agrocomm.agr.br</a></div>
                    </div>

                </div>
               
            </div>
            <div className="text-center">
                        © {new Date().getFullYear()} - Todos os direitos reservados - Agência AGROCOMM 
                </div>
                <hr></hr>
                <div className="text-end p-1 me-4">
                        Desenvolvido por <a href="https://an.tec.br" target="_blank">AN Tecnologia</a>
                </div>
        </footer>
    )
}
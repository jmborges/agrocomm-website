import Link from 'next/link';
import Styles from '../../styles/Header.module.css'
import { useState } from 'react';
export default function Li(props) {
    const [isHovering, setIsHovering] = useState(false);

    const handleMouseIn = () => {
        setIsHovering(true);
    };

    const handleMouseOut = () => {
        setIsHovering(false)
    };
    const allSlugs = [
        {
            slug: 'identidade-visual',
            name: 'Identidade Visual'
        },
        {
            slug: 'projeto-grafico-e-diagramacao',
            name: 'Projeto Gráfico e diagramação'
        },
        {
            slug: 'branding-gestao-de-marcas',
            name: 'Branding (Gestão de Marcas)'
        },
        {
            slug: 'planejamento-e-criacao-de-pecas',
            name: 'Planejamento e Criação de Peças Avulsas'
        },
        {
            slug: 'marketing-digital',
            name: 'Marketing Digital'
        },
        {
            slug: 'consultoria-por-projeto',
            name: 'Consultoria por Projeto'
        },
    ]
    return ( 
        <li className="nav-item dropdown">
            <a className={Styles.nav_link} href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false" onMouseEnter={handleMouseIn} onMouseLeave={handleMouseOut}>
                Nossa Atuação
            </a>
            <ul className={isHovering ? 'dropdown-menu dropdown-menu-white show' : 'dropdown-menu dropdown-menu-dark'} onMouseEnter={handleMouseIn} onMouseLeave={handleMouseOut} aria-labelledby="navbarDarkDropdownMenuLink">
                {allSlugs.map((slug) => (
                    <li key={slug.name}><Link href={`/nossa-atuacao/${slug.slug}/`}><a className={Styles.nav_dropdown_item}>{slug.name}</a></Link></li>
                ))
                }
            </ul>
        </li>
    )
}
import Styles from '../../styles/NossaAtuacao.module.css'
import ContactRow from '../../components/ContactRow'
import Link from 'next/link'
export default function ConsultoriaProjeto() {
    return (
        <>
            <section>
                <div className={Styles.backgroud_orange}>
                </div>
            </section>
            <section className={Styles.backgroud_grey}>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className="row">
                                <div className={Styles.page_title}>
                                    <h1>Consultoria por Projeto em marketing e comunicação</h1>
                                </div>
                                <article className={Styles.container_page_post}>
                                    <div className='row justify-content-end mt-5'>
                                        <div className="col-3">
                                            <div className={Styles.line_orange}></div>
                                        </div>
                                        <div className="col-9">
                                            <p>Prestação de serviços de análise, planejamento e orientação para a implementação de estratégias de marketing e comunicação. Uma modalidade de consultoria por projeto desenvolvido, que pode ajudar sua equipe de marketing a planejar, aplicar e gerenciar, com segurança, estratégias e táticas de comunicação adequadas ao seu ramo de atuação.</p>
                                            <Link href={'/'}><a><button className={Styles.button_back}>{'<'} Voltar</button></a></Link>
                                        </div>
                                    </div>

                                </article>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <ContactRow />
        </>
    )
}

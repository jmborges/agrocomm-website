import Styles from '../../styles/NossaAtuacao.module.css'
import ContactRow from '../../components/ContactRow'
import Link from 'next/link'
export default function IdentidadeVisual() {
    return ( 
        <>
            <section>
                <div className={Styles.backgroud_orange}>
                </div>
            </section>
            <section className={Styles.backgroud_grey}>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className="row">
                                <div className={Styles.page_title}>
                                    <h1>Identidade Visual</h1>
                                </div>
                                <article className={Styles.container_page_post}>
                                    <div className='row justify-content-end mt-5'>
                                        <div className="col-3">
                                            <div className={Styles.line_orange}></div>
                                        </div>
                                        <div className="col-9">
                                            <p>A identidade visual de uma marca consiste no nome, logotipo, fonte, cor e estilo próprio. Ela deve transmitir a essência da história da empresa e os seus valores. A identidade visual é imprescindível para a presença da marca tanto nas mídias tradicionais quanto nas digitais, além de ser um dos pilares de sustentação da estratégia de comunicação da marca com o mercado.</p>
                                            <p>A Agrocomm desenvolve projetos de Identidade Visual para marcas através diagnóstico e estudo precisos que levam em conta o perfil da empresa, o mercado de atuação, a linha de produtos/serviços e o perfil do consumidor.</p>
                                            <Link href={'/'}><a><button className={Styles.button_back}>{'<'} Voltar</button></a></Link>
                                        </div>
                                    </div>
                                    
                                </article>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <ContactRow />
        </>
    )
}

import Styles from '../../styles/NossaAtuacao.module.css'
import ContactRow from '../../components/ContactRow'
import Link from 'next/link'
export default function MarketingDigital() {
    return ( 
        <>
            <section>
                <div className={Styles.backgroud_orange}>
                </div>
            </section>
            <section className={Styles.backgroud_grey}>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className="row">
                                <div className={Styles.page_title}>
                                    <h1>Marketing Digital</h1>
                                </div>
                                <article className={Styles.container_page_post}>
                                    <div className='row justify-content-end mt-5'>
                                        <div className="col-3">
                                            <div className={Styles.line_orange}></div>
                                        </div>
                                        <div className="col-9">
                                            <p>É o conjunto de atividades que a empresa executa online com o objetivo de atrair novos negócios, criar relacionamentos e desenvolver uma identidade de marca. Podemos ajudar sua empresa a desenvolver estratégias de comunicação adequadas através de um diagnóstico e planejamento da comunicação digital, incluindo a análise de perfil do público-alvo, sempre levando em conta as necessidades e objetivos da sua empresa no ambiente virtual.</p>
                                            <Link href={'/'}><a><button className={Styles.button_back}>{'<'} Voltar</button></a></Link>
                                        </div>
                                    </div>
                                    
                                </article>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <ContactRow />
        </>
    )
}
 
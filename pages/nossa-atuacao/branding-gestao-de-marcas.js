import Styles from '../../styles/NossaAtuacao.module.css'
import ContactRow from '../../components/ContactRow'
import Link from 'next/link'
export default function Branding() {
    return ( 
        <>
            <section>
                <div className={Styles.backgroud_orange}>
                </div>
            </section>
            <section className={Styles.backgroud_grey}>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className="row">
                                <div className={Styles.page_title}>
                                    <h1>Branding (Gestão de Marcas)</h1>
                                </div>
                                <article className={Styles.container_page_post}>
                                    <div className='row justify-content-end mt-5'>
                                        <div className="col-3">
                                            <div className={Styles.line_orange}></div>
                                        </div>
                                        <div className="col-9">
                                            <p>É um conjunto de estratégias que visam tornar a marca mais reconhecida pelo seu público e com presença mais destacada no mercado. Seu objetivo principal é despertar interesse e criar conexões fortes, que serão fatores relevantes para a escolha do consumidor pela sua marca no momento de decisão de compra. A adequada gestão de marcas proporciona o aumento da confiança, potencializa a presença da marca na mente do consumidor e gera fidelidade.</p>
                                            <p>Estamos preparados para ajudar sua empresa/marca a planejar e executar as estratégias necessárias para posicioná-la com eficiência e eficácia nos mercados onde atua.</p>
                                            <Link href={'/'}><a><button className={Styles.button_back}>{'<'} Voltar</button></a></Link>
                                        </div>
                                    </div>
                                    
                                </article>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <ContactRow />
        </>
    )
}
 

 
import Styles from '../../styles/NossaAtuacao.module.css'
import ContactRow from '../../components/ContactRow'
import Link from 'next/link'
export default function PlanejamentoCriacao() {
    return ( 
        <>
            <section>
                <div className={Styles.backgroud_orange}>
                </div>
            </section>
            <section className={Styles.backgroud_grey}>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className="row">
                                <div className={Styles.page_title}>
                                    <h1>Planejamento e Criação de Peças Avulsas</h1>
                                </div>
                                <article className={Styles.container_page_post}>
                                    <div className='row justify-content-end mt-5'>
                                        <div className="col-3">
                                            <div className={Styles.line_orange}></div>
                                        </div>
                                        <div className="col-9">
                                            <p>Também atuamos no planejamento e criação de peças avulsas de comunicação destinadas as mais variadas situações e necessidades de sua empresa. Planejamento e criação de anúncios para mídia impressa, roteiros para mídias sonoras e audiovisuais, assessoramento para seleção e contratação de produtoras de áudio e vídeo, criação de  materiais para divulgação e sinalização de dias de campo e eventos corporativos, entre outras peças.</p>
                                            <Link href={'/'}><a><button className={Styles.button_back}>{'<'} Voltar</button></a></Link>
                                        </div>
                                    </div>
                                    
                                </article>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <ContactRow />
        </>
    )
}
 
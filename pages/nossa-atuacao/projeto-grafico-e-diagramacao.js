import Styles from '../../styles/NossaAtuacao.module.css'
import ContactRow from '../../components/ContactRow'
import Link from 'next/link'
export default function ProjetoGrafico() {
    return ( 
        <>
            <section>
                <div className={Styles.backgroud_orange}>
                </div>
            </section>
            <section className={Styles.backgroud_grey}>
                <div className='container'>
                    <div className='row justify-content-center'>
                        <div className='col-md-8'>
                            <div className="row">
                                <div className={Styles.page_title}>
                                    <h1>Projeto Gráfico e Diagramação</h1>
                                </div>
                                <article className={Styles.container_page_post}>
                                    <div className='row justify-content-end mt-5'>
                                        <div className="col-3">
                                            <div className={Styles.line_orange}></div>
                                        </div>
                                        <div className="col-9">
                                            <p>O projeto gráfico para divulgação de conteúdos editoriais seja um periódico ou material de ocasião, precisa estar adequado ao tipo de conteúdo e ao público-alvo a que se destina. Se a apresentação visual desses materiais não for adequada, pode prejudicar a comunicação e todo o trabalho de elaboração do conteúdo se perde. É importante que o público-alvo seja atraído visualmente desde o primeiro momento, por isso, um bom projeto gráfico e diagramação são tão importantes.</p>
                                            <p>Trabalhamos em projetos gráficos e diagramação de jornais, revistas, manuais, portfólios, relatórios, livros e materiais técnicos, entre outros. Valorize ainda mais o conteúdo ofertado aos seus clientes com um visual agradável e tecnicamente adequado.</p>
                                            <Link href={'/'}><a><button className={Styles.button_back}>{'<'} Voltar</button></a></Link>
                                        </div>
                                    </div>
                                    
                                </article>
                            </div>
                        </div>
                    </div>

                </div>
            </section>
            <ContactRow />
        </>
    )
}
 

 
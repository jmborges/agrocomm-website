import AboutRow from '../components/index/AboutRow'
import ActionRow from '../components/index/ActingRow'
import ServicesRow from '../components/index/ServicesRow'
import LogoRow from '../components/index/LogoRow'
import ContactRow from '../components/ContactRow'
export default function Home() {
  return (
    <>
    <AboutRow />
    <ActionRow/>
    <ServicesRow/>
    <LogoRow/>
    <ContactRow/>
    </>
  )
}

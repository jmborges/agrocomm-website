// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
export default function handler(req, res) {
  res.status(200).json([
    { 
      slug: 'identidade-visual',
      name: 'Identidade Visual',
      text: 'A identidade visual de uma marca consiste no nome, logotipo, fonte, cor e estilo próprio. Ela deve transmitir a essência da história da empresa e os seus valores. A identidade visual é imprescindível para a presença da marca tanto nas mídias tradicionais quanto nas digitais, além de ser um dos pilares de sustentação da estratégia de comunicação da marca com o mercado.',
      textTwo: 'A Agrocomm desenvolve projetos de Identidade Visual para marcas através diagnóstico e estudo precisos que levam em conta o perfil da empresa, o mercado de atuação, a linha de produtos/serviços e o perfil do consumidor.'
    },
    {
      slug: 'projeto-grafico-e-diagramacao',
      name: 'Projeto Gráfico e Diagramação',
      text: 'O projeto gráfico para divulgação de conteúdos editoriais seja um periódico ou material de ocasião, precisa estar adequado ao tipo de conteúdo e ao público-alvo a que se destina. Se a apresentação visual desses materiais não for adequada, pode prejudicar a comunicação e todo o trabalho de elaboração do conteúdo se perde. É importante que o público-alvo seja atraído visualmente desde o primeiro momento, por isso, um bom projeto gráfico e diagramação são tão importantes.',
      textTwo: 'Trabalhamos em projetos gráficos e diagramação de jornais, revistas, manuais, portfólios, relatórios, livros e materiais técnicos, entre outros. Valorize ainda mais o conteúdo ofertado aos seus clientes com um visual agradável e tecnicamente adequado. '
    },
    {
      slug: 'branding-gestao-de-marcas',
      name: 'Branding (Gestão de Marcas)',
      text: 'É um conjunto de estratégias que visam tornar a marca mais reconhecida pelo seu público e com presença mais destacada no mercado. Seu objetivo principal é despertar interesse e criar conexões fortes, que serão fatores relevantes para a escolha do consumidor pela sua marca no momento de decisão de compra. A adequada gestão de marcas proporciona o aumento da confiança, potencializa a presença da marca na mente do consumidor e gera fidelidade.',
      textTwo: 'Estamos preparados para ajudar sua empresa/marca a planejar e executar as estratégias necessárias para posicioná-la com eficiência e eficácia nos mercados onde atua.'
    },
    {
      slug: 'planejamento-e-criacao-de-pecas',
      name: 'Planejamento e Criação de Peças Avulsas',
      text: 'Também atuamos no planejamento e criação de peças avulsas de comunicação destinadas as mais variadas situações e necessidades de sua empresa. Planejamento e criação de anúncios para mídia impressa, roteiros para mídias sonoras e audiovisuais, assessoramento para seleção e contratação de produtoras de áudio e vídeo, criação de  materiais para divulgação e sinalização de dias de campo e eventos corporativos, entre outras peças.'
    },
    {
      slug: 'marketing-digital',
      name: 'Marketing Digital',
      text: 'É o conjunto de atividades que a empresa executa online com o objetivo de atrair novos negócios, criar relacionamentos e desenvolver uma identidade de marca. Podemos ajudar sua empresa a desenvolver estratégias de comunicação adequadas através de um diagnóstico e planejamento da comunicação digital, incluindo a análise de perfil do público-alvo, sempre levando em conta as necessidades e objetivos da sua empresa no ambiente virtual.'
    },
    {
      slug: 'consultoria-por-projeto',
      name: 'Consultoria por Projeto em marketing e comunicação',
      text: 'Prestação de serviços de análise, planejamento e orientação para a implementação de estratégias de marketing e comunicação. Uma modalidade de consultoria por projeto desenvolvido, que pode ajudar sua equipe de marketing a planejar, aplicar e gerenciar, com segurança, estratégias e táticas de comunicação adequadas ao seu ramo de atuação.'
    },
  ])
}

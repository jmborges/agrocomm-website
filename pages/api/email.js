import nodemailer from "nodemailer";

export default async (req, res) => {

  const { name, telephone, email, message} = req.body;
  const transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: true,
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD,
    }
  }); 

  try {
    await transporter.sendMail({
      from: process.env.MAIL_USERNAME,
      to: "atendimento@agrocomm.agr.br",
      subject: 'Novo contato no formulário - AGROCOMM',
      html: `<p>Você tem um envio de formulário de contato</p><br>
        <p><strong>Nome: </strong> ${name}</p><br>
        <p><strong>Email: </strong> ${email}</p><br>
        <p><strong>Telefone: </strong> ${telephone}</p><br>
        <p><strong>Messagem: </strong> ${message}</p><br>
      `
    });
  } catch (error) {
    return res.status(500).json({ error: error.message || error.toString() });
  }
  return res.status(200).json({ error: "" });
};